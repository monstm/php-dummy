# Corporate

---

## Corporate Interface

Describes Corporate interface.

### company

Retrieve random company.

```php
$company = $random->company();
```

### department

Retrieve random department.

```php
$department = $random->department();
```

### product

Retrieve random product.

```php
$product = $random->product();
```

### profession

Retrieve random profession.

```php
$profession = $random->profession();
```
