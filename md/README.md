# PHP Dummy

[
	![](https://badgen.net/packagist/v/samy/dummy/latest)
	![](https://badgen.net/packagist/license/samy/dummy)
	![](https://badgen.net/packagist/dt/samy/dummy)
	![](https://badgen.net/packagist/favers/samy/dummy)
](https://packagist.org/packages/samy/dummy)

Random data generator.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require --dev samy/dummy
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-dummy>
* User Manual: <https://monstm.gitlab.io/php-dummy/>
* Documentation: <https://monstm.alwaysdata.net/php-dummy/>
* Issues: <https://gitlab.com/monstm/php-dummy/-/issues>
