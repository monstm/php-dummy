# Data Provider

---

## Data Provider Interface

Describes Data Provider interface.

### option

Retrieve one of value from array element.

```php
$value = $random->option($array);
```

### dataProvider

Retrieve value from data provider.

```php
$value = $random->dataProvider($name, $size);
```

---

## Build-In Data Provider

| Name       | Description                        |
| ---------- | ---------------------------------- |
| address    | Address data collections.          |
| city       | City data collections.             |
| company    | Company data collections.          |
| country    | Country data collections.          |
| department | Department data collections.       |
| name       | Name data collections.             |
| phone-code | Phone code data collections.       |
| product    | Product data collections.          |
| profession | Profession data collections.       |
| tld        | Top level domain data collections. |
| word       | Word data collections.             |
