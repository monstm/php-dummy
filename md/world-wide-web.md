# World Wide Web

---

## World Wide Web Interface

Describes World Wide Web interface.

### tld

Retrieve random top level domain.

```php
$tld = $random->tld();
```

### domain

Retrieve random domain name.

```php
$domain = $random->domain();
```

### website

Retrieve random website name.

```php
$website = $random->website();
```

### url

Retrieve random url.

```php
$url = $random->url();
```
