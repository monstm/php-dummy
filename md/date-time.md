# Date Time

---

## Date Time Interface

Describes Date Time interface.

### date

Retrieve random date.

```php
$date = $random->date($from = date("Y-m-d", strtotime("-5 years")), $to = date("Y-m-d", strtotime("+5 years")));
```

### time

Retrieve random time.

```php
$time = $random->time();
```

### datetime

Retrieve random datetime.

```php
$datetime = $random->datetime($from = date("Y-m-d H:i:s", strtotime("-5 years")), $to = date("Y-m-d H:i:s", strtotime("+5 years")));
```

### timestamp

Retrieve random timestamp.

```php
$timestamp = $random->timestamp($from = strtotime("-5 years"), $to = strtotime("+5 years"));
```
