# Contact

---

## Contact Interface

Describes Contact interface.

### address

Retrieve random address.

```php
$address = $random->address();
```

### city

Retrieve random city.

```php
$city = $random->city();
```

### country

Retrieve random country.

```php
$country = $random->country();
```

### postal

Retrieve random postal.

```php
$postal = $random->postal();
```

### phone

Retrieve random phone.

```php
$phone = $random->phone();
```

### email

Retrieve random email.

```php
$email = $random->email();
```
