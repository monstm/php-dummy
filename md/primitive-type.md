# Primitive Type

---

## Primitive Type Interface

Describes Primitive Type interface.

### boolean

Retrieve random boolean.

```php
$boolean = $random->boolean();
```

### integer

Retrieve random integer.

```php
$integer = $random->integer($min = PHP_INT_MIN, $max = PHP_INT_MAX);
```

### float

Retrieve random float.

```php
$float = $random->float($min = PHP_FLOAT_MIN, $max = PHP_FLOAT_MAX, $precision = 2);
```

### string

Retrieve random string.

```php
$string = $random->string($length = 8, $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~+/");
```

### mixed

Retrieve random mixed.

```php
$mixed = $random->mixed();
```
