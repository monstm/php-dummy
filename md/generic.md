# Generic

---

## Generic Interface

Describes Generic interface.

### name

Retrieve random name.

```php
$name = $random->name($size = 1);
```

### word

Retrieve random word.

```php
$word = $random->word($size = 1);
```
