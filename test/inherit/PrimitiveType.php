<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractPrimitiveType;

/**
 * Simple PrimitiveType implementation.
 */
class PrimitiveType extends AbstractPrimitiveType
{
}
