<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractContact;

/**
 * Simple Contact implementation.
 */
class Contact extends AbstractContact
{
}
