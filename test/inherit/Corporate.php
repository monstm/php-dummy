<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractCorporate;

/**
 * Simple Corporate implementation.
 */
class Corporate extends AbstractCorporate
{
}
