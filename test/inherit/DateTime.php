<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractDateTime;

/**
 * Simple DateTime implementation.
 */
class DateTime extends AbstractDateTime
{
}
