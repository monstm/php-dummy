<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractDataProvider;

/**
 * Simple DataProvider implementation.
 */
class DataProvider extends AbstractDataProvider
{
}
