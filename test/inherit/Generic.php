<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractGeneric;

/**
 * Simple Generic implementation.
 */
class Generic extends AbstractGeneric
{
}
