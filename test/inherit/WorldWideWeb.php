<?php

namespace Test\Inherit;

use Samy\Dummy\Abstract\AbstractWorldWideWeb;

/**
 * Simple WorldWideWeb implementation.
 */
class WorldWideWeb extends AbstractWorldWideWeb
{
}
