<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\DateTime;

class DateTimeTest extends AbstractTestCase
{
    /** @var DateTime */
    protected $date_time;

    protected function setUp(): void
    {
        $this->date_time = new DateTime();
    }

    /**
     * Test date.
     *
     * @return void
     */
    public function testDate(): void
    {
        $from = "2020-02-02";
        $to = "2023-03-03";
        $result = $this->date_time->date($from, $to);

        $this->assertRange(strtotime($result), strtotime($from), strtotime($to));
    }

    /**
     * Test time.
     *
     * @return void
     */
    public function testTime(): void
    {
        $this->assertRange(strtotime($this->date_time->time()), strtotime("00:00:00"), strtotime("23:59:59"));
    }

    /**
     * Test datetime.
     *
     * @return void
     */
    public function testDatetime(): void
    {
        $from = "2020-02-02 02:02:02";
        $to = "2023-03-03 03:03:03";
        $result = $this->date_time->datetime($from, $to);

        $this->assertRange(strtotime($result), strtotime($from), strtotime($to));
    }

    /**
     * Test timestamp.
     *
     * @return void
     */
    public function testTimestamp(): void
    {
        $from = "2020-02-02 02:02:02";
        $to = "2023-03-03 03:03:03";
        $result = $this->date_time->timestamp($from, $to);

        $this->assertRange($result, strtotime($from), strtotime($to));
    }

    /**
     * assert range.
     *
     * @param mixed $Actual The actual value.
     * @param mixed $Lowest The lowest value.
     * @param mixed $Highest The highest value.
     * @return void
     */
    private function assertRange(mixed $Actual, mixed $Lowest, mixed $Highest): void
    {
        $this->assertGreaterThanOrEqual($Lowest, $Actual);
        $this->assertLessThanOrEqual($Highest, $Actual);
    }
}
