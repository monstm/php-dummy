<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Corporate;

class CorporateTest extends AbstractTestCase
{
    /** @var Corporate */
    protected $corporate;

    protected function setUp(): void
    {
        $this->corporate = new Corporate();
    }

    /**
     * Test company.
     *
     * @return void
     */
    public function testCompany(): void
    {
        $this->assertIsString($this->corporate->company());
    }

    /**
     * Test department.
     *
     * @return void
     */
    public function testDepartment(): void
    {
        $this->assertIsString($this->corporate->department());
    }

    /**
     * Test product.
     *
     * @return void
     */
    public function testProduct(): void
    {
        $this->assertIsString($this->corporate->product());
    }

    /**
     * Test profession.
     *
     * @return void
     */
    public function testProfession(): void
    {
        $this->assertIsString($this->corporate->profession());
    }
}
