<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Contact;

class ContactTest extends AbstractTestCase
{
    /** @var Contact */
    protected $contact;

    protected function setUp(): void
    {
        $this->contact = new Contact();
    }

    /**
     * Test address.
     *
     * @return void
     */
    public function testAddress(): void
    {
        $this->assertIsString($this->contact->address());
    }

    /**
     * Test city.
     *
     * @return void
     */
    public function testCity(): void
    {
        $this->assertIsString($this->contact->city());
    }

    /**
     * Test country.
     *
     * @return void
     */
    public function testCountry(): void
    {
        $this->assertIsString($this->contact->country());
    }

    /**
     * Test postal.
     *
     * @return void
     */
    public function testPostal(): void
    {
        $this->assertIsString($this->contact->postal());
    }

    /**
     * Test phone.
     *
     * @return void
     */
    public function testPhone(): void
    {
        $this->assertIsString($this->contact->phone());
    }

    /**
     * Test email.
     *
     * @return void
     */
    public function testUrl(): void
    {
        $email = $this->contact->email();
        $filter_var = filter_var($email, FILTER_VALIDATE_EMAIL);

        $this->assertIsString($filter_var);
    }
}
