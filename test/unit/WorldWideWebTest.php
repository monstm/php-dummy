<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\WorldWideWeb;

class WorldWideWebTest extends AbstractTestCase
{
    /** @var WorldWideWeb */
    protected $world_wide_web;

    protected function setUp(): void
    {
        $this->world_wide_web = new WorldWideWeb();
    }

    /**
     * Test tld.
     *
     * @return void
     */
    public function testTld(): void
    {
        $this->assertIsString($this->world_wide_web->tld());
    }

    /**
     * Test domain.
     *
     * @return void
     */
    public function testDomain(): void
    {
        $domain = $this->world_wide_web->domain();
        $filter_var = filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME);

        $this->assertIsString($filter_var);
    }

    /**
     * Test website.
     *
     * @return void
     */
    public function testWebsite(): void
    {
        $website = $this->world_wide_web->website();
        $filter_var = filter_var($website, FILTER_VALIDATE_URL);

        $this->assertIsString($filter_var);
    }

    /**
     * Test url.
     *
     * @return void
     */
    public function testUrl(): void
    {
        $website = $this->world_wide_web->website();
        $filter_var = filter_var($website, FILTER_VALIDATE_URL);

        $this->assertIsString($filter_var);
    }
}
