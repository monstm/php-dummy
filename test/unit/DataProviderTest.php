<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\DataProvider;

class DataProviderTest extends AbstractTestCase
{
    /** @var DataProvider */
    protected $data_provider;

    protected function setUp(): void
    {
        $this->data_provider = new DataProvider();
    }

    /**
     * Test option.
     *
     * @return void
     */
    public function testOption(): void
    {
        $expect = ["null", "boolean", "integer", "double", "string", "array", "object"];
        $actual = $this->data_provider->option($expect);

        $this->assertContains($actual, $expect);
    }

    /**
     * Test data provider.
     *
     * @return void
     */
    public function testDataProvider(): void
    {
        $data_directory = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . "data-provider";
        $data_names = $this->lst(__DIR__ . DIRECTORY_SEPARATOR . "data-provider.lst");

        foreach ($data_names as $name) {
            $expect = $this->lst($data_directory . DIRECTORY_SEPARATOR . $name . ".lst");
            $actual = $this->data_provider->dataProvider($name);

            $this->assertContains($actual, $expect);
        }
    }
}
