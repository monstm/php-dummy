<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\PrimitiveType;

class PrimitiveTypeTest extends AbstractTestCase
{
    /** @var PrimitiveType */
    protected $primitive_type;

    protected function setUp(): void
    {
        $this->primitive_type = new PrimitiveType();
    }

    /**
     * Test boolean.
     *
     * @return void
     */
    public function testBoolean(): void
    {
        $this->assertIsBool($this->primitive_type->boolean());
    }

    /**
     * Test integer.
     *
     * @return void
     */
    public function testInteger(): void
    {
        $this->assertIsInt($this->primitive_type->integer());
    }

    /**
     * Test float.
     *
     * @return void
     */
    public function testFloat(): void
    {
        $this->assertIsFloat($this->primitive_type->float());
    }

    /**
     * Test string.
     *
     * @return void
     */
    public function testString(): void
    {
        $this->assertIsString($this->primitive_type->string());
    }

    /**
     * Test mixed.
     *
     * @return void
     */
    public function testMixed(): void
    {
        $value = $this->primitive_type->mixed();
        $actual = strtolower(gettype($value));
        $option = ["null", "boolean", "integer", "double", "string", "array", "object"];

        $this->assertContains($actual, $option);
    }
}
