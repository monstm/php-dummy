<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Generic;

class GenericTest extends AbstractTestCase
{
    /** @var Generic */
    protected $generic;

    protected function setUp(): void
    {
        $this->generic = new Generic();
    }

    /**
     * Test name.
     *
     * @return void
     */
    public function testName(): void
    {
        $this->assertIsString($this->generic->name());
    }

    /**
     * Test word.
     *
     * @return void
     */
    public function testWord(): void
    {
        $this->assertIsString($this->generic->word());
    }
}
