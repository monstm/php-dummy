<?php

namespace Samy\Dummy\Abstract;

use Samy\Dummy\Interface\ContactInterface;

/**
 * This is a simple Contact implementation that other Contact can inherit from.
 */
abstract class AbstractContact extends AbstractWorldWideWeb implements ContactInterface
{
    /**
     * Retrieve random address.
     *
     * @return string
     */
    public function address(): string
    {
        return  $this->integer(1, 99) . " " . $this->dataProvider("address");
    }

    /**
     * Retrieve random city.
     *
     * @return string
     */
    public function city(): string
    {
        return $this->dataProvider("city");
    }

    /**
     * Retrieve random country.
     *
     * @return string
     */
    public function country(): string
    {
        return $this->dataProvider("country");
    }

    /**
     * Retrieve random postal.
     *
     * @return string
     */
    public function postal(): string
    {
        return $this->string(1, "123456789") .  $this->string(4, "0123456789");
    }

    /**
     * Retrieve random phone.
     *
     * @return string
     */
    public function phone(): string
    {
        $prefix = $this->dataProvider("phone-code");
        $affix = $this->string(3, "123456789");
        $size = $this->integer(10, 13);
        $space = $size - strlen($prefix . $affix);
        $suffix = $this->string($space, "0123456789");

        return $prefix . $affix . $suffix;
    }

    /**
     * Retrieve random email.
     *
     * @return string
     */
    public function email(): string
    {
        return strtolower($this->name()) . "@" . strtolower($this->domain());
    }
}
