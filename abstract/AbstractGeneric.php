<?php

namespace Samy\Dummy\Abstract;

use Samy\Dummy\Interface\GenericInterface;

/**
 * This is a simple Generic implementation that other Generic can inherit from.
 */
abstract class AbstractGeneric extends AbstractDataProvider implements GenericInterface
{
    /**
     * Retrieve random name.
     *
     * @param int $Size The size of name to return.
     * @return string
     */
    public function name(int $Size = 1): string
    {
        return $this->dataProvider("name", $Size);
    }

    /**
     * Retrieve random word.
     *
     * @param int $Size The size of word to return.
     * @return string
     */
    public function word(int $Size = 1): string
    {
        return $this->dataProvider("word", $Size);
    }
}
