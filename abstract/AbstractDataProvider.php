<?php

namespace Samy\Dummy\Abstract;

use Samy\DataProvider\DataProvider;
use Samy\Dummy\Interface\DataProviderInterface;

/**
 * This is a simple Data Provider implementation that other Data Provider can inherit from.
 */
abstract class AbstractDataProvider extends AbstractDateTime implements DataProviderInterface
{
    /**
     * Retrieve one of value from array element.
     *
     * @param array<mixed> $Data The options.
     * @return mixed
     */
    public function option(array $Data = []): mixed
    {
        $pointer = array_rand($Data);

        return $Data[$pointer];
    }

    /**
     * Retrieve value from data provider.
     *
     * @param string $Name The build-in data provider name.
     * @param int $Size The size of name to return.
     * @return string
     */
    public function dataProvider(string $Name, int $Size = 1): string
    {
        $ret = [];
        $filename = dirname(__DIR__) . DIRECTORY_SEPARATOR . "data-provider" . DIRECTORY_SEPARATOR . $Name . ".lst";
        $data = DataProvider::lst($filename);

        for ($index = 0; $index < $Size; $index++) {
            array_push($ret, $this->option($data));
        }

        return implode(" ", $ret);
    }

    /**
     * Retrieve values of data provider.
     *
     * @param string $Name The build-in data provider name.
     * @return array<string>
     */
    protected function getDataProvider(string $Name): array
    {
        $ret = [];
        $filename = dirname(__DIR__) . DIRECTORY_SEPARATOR . "data-provider" . DIRECTORY_SEPARATOR . $Name . ".lst";

        if (!is_file($filename)) {
            return $ret;
        }

        $content = @file_get_contents($filename);
        if (!is_string($content)) {
            return $ret;
        }

        return explode("\n", $content);
    }
}
