<?php

namespace Samy\Dummy\Abstract;

use Samy\Dummy\Interface\WorldWideWebInterface;

/**
 * This is a simple World Wide Web implementation that other World Wide Web can inherit from.
 */
abstract class AbstractWorldWideWeb extends AbstractCorporate implements WorldWideWebInterface
{
    /**
     * Retrieve random top level domain.
     *
     * @return string
     */
    public function tld(): string
    {
        return $this->dataProvider("tld");
    }

    /**
     * Retrieve random domain name.
     *
     * @return string
     */
    public function domain(): string
    {
        $sld = preg_replace("/[^a-z]/", "-", strtolower($this->company()));
        return $sld . "." . $this->tld();
    }

    /**
     * Retrieve random website name.
     *
     * @return string
     */
    public function website(): string
    {
        return ($this->boolean() ? "https" : "http") . "://" . $this->domain();
    }

    /**
     * Retrieve random url.
     *
     * @return string
     */
    public function url(): string
    {
        $website = $this->website();
        $path = $this->getUrlPath();
        $query = $this->getUrlQuery();

        return $website . $path . $query;
    }

    /**
     * Retrieve random url path.
     *
     * @return string
     */
    private function getUrlPath(): string
    {
        $ret = "/";
        if (!$this->boolean()) {
            return $ret;
        }

        $words = $this->word($this->integer(1, 2));
        $uri = preg_replace("/[^a-z ]/", "-", strtolower($words));
        if (!is_string($uri)) {
            return $ret;
        }

        $end_of_path = ($this->boolean() ? "" : "/");
        $ret .= str_replace(" ", "/", $uri) . $end_of_path;

        return $ret;
    }

    /**
     * Retrieve random url query.
     *
     * @return string
     */
    public function getUrlQuery(): string
    {
        if (!$this->boolean()) {
            return "";
        }

        $data = [];
        $count = $this->integer(1, 3);

        for ($index = 0; $index < $count; $index++) {
            $key = $this->word();
            $data[$key] = $this->word();
        }

        return "?" . http_build_query($data);
    }
}
