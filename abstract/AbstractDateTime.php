<?php

namespace Samy\Dummy\Abstract;

use Samy\Dummy\Interface\DateTimeInterface;

/**
 * This is a simple Date Time implementation that other Date Time can inherit from.
 */
abstract class AbstractDateTime extends AbstractPrimitiveType implements DateTimeInterface
{
    /**
     * Retrieve random date.
     *
     * @param ?string $From The lowest date value.
     * @param ?string $To The highest date value.
     * @return string
     */
    public function date(?string $From = null, ?string $To = null): string
    {
        return date("Y-m-d", $this->timestamp($From, $To));
    }

    /**
     * Retrieve random time.
     *
     * @return string
     */
    public function time(): string
    {
        return date("H:i:s", $this->timestamp("1970-01-01 00:00:00", "1970-01-01 23:59:59"));
    }

    /**
     * Retrieve random datetime.
     *
     * @param ?string $From The lowest date value.
     * @param ?string $To The highest date value.
     * @return string
     */
    public function datetime(?string $From = null, ?string $To = null): string
    {
        return date("Y-m-d H:i:s", $this->timestamp($From, $To));
    }

    /**
     * Retrieve random timestamp.
     *
     * @param ?string $From The lowest date value.
     * @param ?string $To The highest date value.
     * @return int
     */
    public function timestamp(?string $From = null, ?string $To = null): int
    {
        if (is_null($From)) {
            $From = "-5 years";
        }

        if (is_null($To)) {
            $To = "+5 years";
        }

        $lowest = strtotime($From);
        $min = (is_int($lowest) ? $lowest : PHP_INT_MIN);

        $highest = strtotime($To);
        $max = (is_int($highest) ? $highest : PHP_INT_MAX);

        return rand($min, $max);
    }
}
