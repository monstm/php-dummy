<?php

namespace Samy\Dummy\Abstract;

use Samy\Dummy\Interface\PrimitiveTypeInterface;

/**
 * This is a simple Primitive Type implementation that other Primitive Type can inherit from.
 */
abstract class AbstractPrimitiveType implements PrimitiveTypeInterface
{
    /**
     * Retrieve random boolean.
     *
     * @return bool
     */
    public function boolean(): bool
    {
        return ((rand() % 2) == 0);
    }

    /**
     * Retrieve random integer.
     *
     * @param int $Min The minimum number.
     * @param int $Max The maximum number.
     * @return int
     */
    public function integer(int $Min = PHP_INT_MIN, int $Max = PHP_INT_MAX): int
    {
        return rand($Min, $Max);
    }

    /**
     * Retrieve random float.
     *
     * @param float $Min The minimum number.
     * @param float $Max The maximum number.
     * @param int $Precision The precision number.
     * @return float
     */
    public function float(float $Min = PHP_FLOAT_MIN, float $Max = PHP_FLOAT_MAX, $Precision = 2): float
    {
        $power = pow(10, $Precision);
        $decimal = rand(0, ($power - 1)) / $power;
        $ceil = intval(ceil($Min));
        $floor = intval(floor($Max));
        $rand_min = min($ceil, $floor);
        $rand_max = max($ceil, $floor);
        $ret = rand($rand_min, $rand_max) + $decimal;

        return max(min($ret, $Max), $Min);
    }

    /**
     * Retrieve random string.
     *
     * @param int $Length The character length.
     * @param string $Characters The character possibility.
     * @return string
     */
    public function string(
        int $Length = 8,
        string $Characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~+/"
    ): string {
        $ret = "";
        $limit = strlen($Characters) - 1;

        for ($index = 0; $index < $Length; $index++) {
            $pointer = rand(0, $limit);
            $character = substr($Characters, $pointer, 1);
            $ret .= $character;
        }

        return $ret;
    }

    /**
     * Retrieve random mixed.
     *
     * @return mixed
     */
    public function mixed(): mixed
    {
        $type = rand() % 7;

        switch ($type) {
            case 1:
                $ret = $this->boolean();
                break;
            case 2:
                $ret = $this->integer();
                break;
            case 3:
                $ret = $this->float();
                break;
            case 4:
                $ret = $this->string();
                break;
            case 5:
                $ret = [];
                break;
            case 6:
                $ret = json_decode("{}");
                break;
            default:
                $ret = null;
                break;
        }

        return $ret;
    }
}
