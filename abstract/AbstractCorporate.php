<?php

namespace Samy\Dummy\Abstract;

use Samy\Dummy\Interface\CorporateInterface;

/**
 * This is a simple Corporate implementation that other Corporate can inherit from.
 */
abstract class AbstractCorporate extends AbstractGeneric implements CorporateInterface
{
    /**
     * Retrieve random company.
     *
     * @return string
     */
    public function company(): string
    {
        return $this->dataProvider("company");
    }

    /**
     * Retrieve random department.
     *
     * @return string
     */
    public function department(): string
    {
        return $this->dataProvider("department");
    }

    /**
     * Retrieve random product.
     *
     * @return string
     */
    public function product(): string
    {
        return $this->dataProvider("product");
    }

    /**
     * Retrieve random profession.
     *
     * @return string
     */
    public function profession(): string
    {
        return $this->dataProvider("profession");
    }
}
