<?php

/**
 * @return array<string,mixed>
 */
function getDataProviderList(): array
{
    $ret = [];
    $scandir = scandir(DATA_PROVIDER_LIST_PATH);

    if (!is_array($scandir)) {
        return $ret;
    }

    foreach ($scandir as $filename) {
        $path = DATA_PROVIDER_LIST_PATH . DIRECTORY_SEPARATOR . $filename;
        if (!is_file($path)) {
            continue;
        }

        $pathinfo = pathinfo($path);
        if (!isset($pathinfo["extension"]) || ($pathinfo["extension"] != "lst")) {
            continue;
        }

        $name = strtolower($pathinfo["filename"]);
        $ret[$name] = getListData($path);
    }

    return $ret;
}

/**
 * @param string $Filename The list filename
 * @return array<string>
 */
function getListData(string $Filename): array
{
    $ret = [];

    $content = file_get_contents($Filename);
    if (!is_string($content)) {
        return $ret;
    }

    foreach (explode("\n", $content) as $line) {
        $line = trim($line);
        $line = removeRomanPrefix($line);
        $is_empty = empty($line);
        $is_backlist = in_array($line, DATA_PROVIDER_DATA_BLACKLIST);
        $is_illegal_character = preg_match("/^[a-z0-9 .\-]+$/i", $line) != 1;
        if ($is_empty || $is_backlist || $is_illegal_character) {
            continue;
        }

        array_push($ret, $line);
    }

    $ret = array_unique($ret);
    $ret = array_slice($ret, 0, DATA_PROVIDER_DATA_LIMIT);
    sort($ret);

    return $ret;
}

/**
 * @param string $Namespace The namespace
 * @return string
 */
function removeRomanPrefix(string $Namespace): string
{
    $names = explode(" ", $Namespace);
    $pointer = count($names) - 1;
    $romans = ["I", "II", "III", "IV", "V", "VI", "VII", "VII", "IX", "X"];

    if ($pointer > 0) {
        $end_of_name = $names[$pointer];
        if (in_array($end_of_name, $romans)) {
            unset($names[$pointer]);
        }
    }

    return implode(" ", $names);
}

/**
 * @param string $Name The provider name
 * @param array<string> $Data The data provider
 * @return void
 */
function writeDataProviderBuildIn(string $Name, array $Data): void
{
    $path = DATA_PROVIDER_BUILDIN_PATH . DIRECTORY_SEPARATOR . $Name . ".lst";
    $content = implode("\n", $Data);
    file_put_contents($path, $content);
}

/**
 * @param array<string> $ProviderNames The provider names
 * @return void
 */
function writeDataProviderTestList(array $ProviderNames): void
{
    file_put_contents(DATA_PROVIDER_TEST_LIST, implode("\n", $ProviderNames));
}
