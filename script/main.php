<?php

require("consts.php");
require("function.php");

$data_providers = [];
foreach (getDataProviderList() as $name => $data) {
    if (is_array($data)) {
        writeDataProviderBuildIn($name, $data);
        array_push($data_providers, $name);
    }
}

writeDataProviderTestList($data_providers);
