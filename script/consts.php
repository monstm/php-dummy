<?php

define("ROOT_PATH", dirname(__DIR__));

define("DATA_PROVIDER_LIST_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "script" . DIRECTORY_SEPARATOR . "data-provider");
define("DATA_PROVIDER_BUILDIN_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "data-provider");
define("DATA_PROVIDER_TEST_LIST", ROOT_PATH .
    DIRECTORY_SEPARATOR . "test" .
    DIRECTORY_SEPARATOR . "unit" .
    DIRECTORY_SEPARATOR . "data-provider.lst");

define("DATA_PROVIDER_DATA_LIMIT", 100);
define("DATA_PROVIDER_DATA_BLACKLIST", ["1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th"]);
