<?php

namespace Samy\Dummy\Interface;

/**
 * Describes Data Provider interface.
 */
interface DataProviderInterface
{
    /**
     * Retrieve one of value from array element.
     *
     * @param array<mixed> $Data The options.
     * @return mixed
     */
    public function option(array $Data = []): mixed;

    /**
     * Retrieve value from data provider.
     *
     * @param string $Nama The build-in data provider name.
     * @param int $Size The size of data to return.
     * @return string
     */
    public function dataProvider(string $Nama, int $Size = 1): string;
}
