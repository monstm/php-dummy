<?php

namespace Samy\Dummy\Interface;

/**
 * Describes Generic interface.
 */
interface GenericInterface
{
    /**
     * Retrieve random name.
     *
     * @param int $Size The size of name to return.
     * @return string
     */
    public function name(int $Size = 1): string;

    /**
     * Retrieve random word.
     *
     * @param int $Size The size of word to return.
     * @return string
     */
    public function word(int $Size = 1): string;
}
