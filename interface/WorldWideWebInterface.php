<?php

namespace Samy\Dummy\Interface;

/**
 * Describes World Wide Web interface.
 */
interface WorldWideWebInterface
{
    /**
     * Retrieve random top level domain.
     *
     * @return string
     */
    public function tld(): string;

    /**
     * Retrieve random domain name.
     *
     * @return string
     */
    public function domain(): string;

    /**
     * Retrieve random website name.
     *
     * @return string
     */
    public function website(): string;

    /**
     * Retrieve random url.
     *
     * @return string
     */
    public function url(): string;
}
