<?php

namespace Samy\Dummy\Interface;

/**
 * Describes Date Time interface.
 */
interface DateTimeInterface
{
    /**
     * Retrieve random date.
     *
     * @param ?string $From The lowest date value.
     * @param ?string $To The highest date value.
     * @return string
     */
    public function date(?string $From = null, ?string $To = null): string;

    /**
     * Retrieve random time.
     *
     * @return string
     */
    public function time(): string;

    /**
     * Retrieve random datetime.
     *
     * @param ?string $From The lowest date value.
     * @param ?string $To The highest date value.
     * @return string
     */
    public function datetime(?string $From = null, ?string $To = null): string;

    /**
     * Retrieve random timestamp.
     *
     * @param ?string $From The lowest date value.
     * @param ?string $To The highest date value.
     * @return int
     */
    public function timestamp(?string $From = null, ?string $To = null): int;
}
