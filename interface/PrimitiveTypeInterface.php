<?php

namespace Samy\Dummy\Interface;

/**
 * Describes Primitive Type interface.
 */
interface PrimitiveTypeInterface
{
    /**
     * Retrieve random boolean.
     *
     * @return bool
     */
    public function boolean(): bool;

    /**
     * Retrieve random integer.
     *
     * @param int $Min The minimum number.
     * @param int $Max The maximum number.
     * @return int
     */
    public function integer(int $Min = PHP_INT_MIN, int $Max = PHP_INT_MAX): int;

    /**
     * Retrieve random float.
     *
     * @param float $Min The minimum number.
     * @param float $Max The maximum number.
     * @param int $Precision The precision number.
     * @return float
     */
    public function float(float $Min = PHP_FLOAT_MIN, float $Max = PHP_FLOAT_MAX, $Precision = 2): float;

    /**
     * Retrieve random string.
     *
     * @param int $Length The character length.
     * @param string $Characters The character possibility.
     * @return string
     */
    public function string(
        int $Length = 8,
        string $Characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~+/"
    ): string;

    /**
     * Retrieve random mixed.
     *
     * @return mixed
     */
    public function mixed(): mixed;
}
