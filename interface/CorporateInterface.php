<?php

namespace Samy\Dummy\Interface;

/**
 * Describes Corporate interface.
 */
interface CorporateInterface
{
    /**
     * Retrieve random company.
     *
     * @return string
     */
    public function company(): string;

    /**
     * Retrieve random department.
     *
     * @return string
     */
    public function department(): string;

    /**
     * Retrieve random product.
     *
     * @return string
     */
    public function product(): string;

    /**
     * Retrieve random profession.
     *
     * @return string
     */
    public function profession(): string;
}
