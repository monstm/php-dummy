<?php

namespace Samy\Dummy\Interface;

/**
 * Describes Contact interface.
 */
interface ContactInterface
{
    /**
     * Retrieve random address.
     *
     * @return string
     */
    public function address(): string;

    /**
     * Retrieve random city.
     *
     * @return string
     */
    public function city(): string;

    /**
     * Retrieve random country.
     *
     * @return string
     */
    public function country(): string;

    /**
     * Retrieve random postal.
     *
     * @return string
     */
    public function postal(): string;

    /**
     * Retrieve random phone.
     *
     * @return string
     */
    public function phone(): string;

    /**
     * Retrieve random email.
     *
     * @return string
     */
    public function email(): string;
}
